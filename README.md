## WPezClasses: Taxonomy Args

__Create your WordPress custom taxonomy args The ezWay.__
   

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._


    use WPezSuite\WPezClasses\TaxonomyLabelsUS\ClassTaxonomyLabelsUS as Labels;
    use WPezSuite\WPezClasses\TaxonomyArgs\ClassTaxonomyArgs as TaxArgs;
    
    $new_labels = new Labels();
    $new_labels->setNames( 'TEST Tax' );
    $arr_labels = $new_labels->getLabels();
    
    $new_args = new TaxArgs();
    $new_args->setLabels( $arr_labels );
    $new_args->setArgs( array(...) );
    $arr_args = $new_args->getArgsAll();
    
    


There are three public methods:

- loadArgsBase( $arr ) - Allows you to build a library of standard CPT types / configs.

- loadArgs( $arr ) - The particulars of this CTP.

- updateArgs( $arr ) - Another way to load your args.

- getArgsAll( $arr ) - returns array_merge() of ArgsDefaults, ArgsBase, ArrArgs, $arr passed in and/or properties set using the setters. 

The general idea is to give you as much granular control as possible, without having to slow you down with doing your own array_merge()s.



### FAQ

__1) Why?__

In this case, that's a really good question. The taxonomy args are already an array. Perhaps this class isn't all that necessary. None the less, it's self-documenting so referencing the codex for any details is no longer necessary.    


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 

 __3) - What else do we need to know?__
 
In general, WP's defaults are followed but there are exceptions. For example, the default for 'show_in_rest' is true (in the TaxArgs class).


 __4) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyLabelsUS

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyRegistrer

- https://codex.wordpress.org/Function_Reference/register_taxonomy


### TODO

- 


### CHANGE LOG

-v.0.0.3 - Saturday 7 Jan 2023
    - UPDATE: Refactoring + comments of the three WPez repos for WP taxonomies.

- v0.0.1 - Saturday 20 April 2019
    - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 