<?php
/**
 * The ezWay to do WP Taxonomy Args.
 *
 * @package WPezSuite\WPezClasses\TaxonomyArgs
 */

namespace WPezSuite\WPezClasses\TaxonomyArgs;

// No WP? No good.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * WP Tax Args managed via a class.
 */
class ClassTaxonomyArgs {

	/**
	 * Perhaps you have a standard set of args to always override the WP defaults.
	 *
	 * @var array
	 */
	protected $arr_args_base;

	/**
	 * Args for the eventual register_taxonomy().
	 *
	 * @var array
	 */
	protected $arr_args;

	/**
	 * Setters return nothing but the result of a given set is available via this property and getSetReturn().
	 *
	 * @var mixed
	 */
	protected $mix_set_return;

	/**
	 * Label plural.
	 *
	 * @var string
	 */
	protected $str_label_plural;

	/**
	 * An array of labels for this taxonomy. By default, Tag labels are used for non-hierarchical taxonomies, and Category labels are used for hierarchical taxonomies. See accepted values in get_taxonomy_labels().
	 *
	 * @var array
	 */
	protected $arr_labels;

	/**
	 * A short descriptive summary of what the taxonomy is for.
	 *
	 * @var string
	 */
	protected $str_description;

	/**
	 * Whether a taxonomy is intended for use publicly either via the admin interface or by front - end users. The default settings of $publicly_queryable, $show_ui, and $show_in_nav_menus are inherited from $public.
	 *
	 * @var bool
	 */
	protected $bool_public;

	/**
	 * Default: inherited from $public - Whether the taxonomy is publicly queryable.
	 *
	 * @var bool
	 */
	protected $bool_publicly_queryable;

	/**
	 * Default: false - Whether the taxonomy is hierarchical.
	 *
	 * @var bool
	 */
	protected $bool_hierarchical;

	/**
	 * Default: inherited from $public - Whether to generate and allow a UI for managing terms in this taxonomy in the admin.
	 *
	 * @var bool
	 */
	protected $bool_show_ui;

	/**
	 *  Default: inherited from $show_ui, $show_ui must be true - Whether to show the taxonomy in the admin menu. If true, the taxonomy is shown as a submenu of the object type menu. If false, no menu is shown. $show_ui must be true.
	 *
	 * @var bool
	 */
	protected $bool_show_in_menu;

	/**
	 * Default: inherited from $public -  Makes this taxonomy available for selection in navigation menus.I
	 *
	 * @var bool
	 */
	protected $bool_show_in_nav_menus;

	/**
	 * Default: unknown - Whether to require the taxonomy in the REST API. Set this to true for the taxonomy to be available in the block editor.
	 *
	 * @var bool
	 */
	protected $bool_show_in_rest;

	/**
	 * Default: $taxonomy - To change the base url of REST API route.
	 *
	 * @var string
	 */
	protected $str_rest_base;

	/**
	 * Default: wp/v2 -  To change the namespace URL of REST API route. TODO SETTER.
	 *
	 * @var string
	 */
	protected $str_rest_namespace;

	/**
	 * Default: 'WP_REST_Terms_Controller' - REST API Controller class name.
	 *
	 * @var string
	 */
	protected $str_rest_controller_class;

	/**
	 * Default: inherited from $show_ui - Whether to list the taxonomy in the Tag Cloud Widget controls.
	 *
	 * @var bool
	 */
	protected $bool_show_tagcloud;

	/**
	 * Default: inherited from $show_ui. Whether to show the taxonomy in the quick/bulk edit panel.
	 *
	 * @var bool
	 */
	protected $bool_show_in_quick_edit;

	/**
	 * Default: false - Whether to display a column for the taxonomy on its post type listing screens.
	 *
	 * @var bool
	 */
	protected $bool_show_admin_column;

	/**
	 * Default: post_categories_meta_box() for hierarchical taxonomies, and post_tags_meta_box() for non-hierarchical. Provide a callback function for the meta box display. If false, no meta box is shown.
	 *
	 * @var string|false
	 */
	protected $mix_meta_box_cb;

	/**
	 * Callback function for sanitizing taxonomy data saved from a meta box. If no callback is defined, an appropriate one is determined based on the value of $meta_box_cb.
	 *
	 * @var string
	 */
	protected $str_meta_box_sanitize_cb;

	/**
	 * Default: See WP docs - An array of capabilities for this taxonomy.
	 *
	 * @var array
	 */
	protected $arr_capabilities;

	/**
	 * Default: true, using $taxonomy as slug . Triggers the handling of rewrites for this taxonomy. To prevent rewrite, set to false . To specify rewrite rules, an array can be passed with any of these keys: See WP docs
	 *
	 * @var bool|array
	 */
	protected $mix_rewrite;

	/**
	 * Default: $taxonomy key - Sets the query var key for this taxonomy. If false, a taxonomy cannot be loaded at ?{query_var}={term_slug}. If a string, the query ?{query_var}={term_slug} will be valid. Sets the query var key for this taxonomy.
	 *
	 * @var false|string
	 */

	protected $mix_query_var;

	/**
	 * Default: _update_post_term_count() for taxonomies attached to post types, which confirms that the objects are published before counting them or (default) _update_generic_term_count() for taxonomies attached to other object types, such as users. Works much like a hook, in that it will be called when the count is updated.
	 *
	 * @var string
	 */
	protected $str_update_count_callback;

	/**  TODO - setter, etc.
	 * Default term to be used for the taxonomy.
	 *
	 * @var string|array
	 */
	protected $mix_default_term;

	/**
	 * Default: null which equates to false - Whether terms in this taxonomy should be sorted in the order they are provided to wp_set_object_terms().
	 *
	 * @var bool
	 */
	protected $bool_sort;

	/**
	 * Default: null - Array of arguments to automatically use inside wp_get_object_terms() for this taxonomy.
	 *
	 * @var array
	 */
	protected $arr_wp_get_object_terms_args;

	/**
	 * Default: false - This taxonomy is a 'built-in' taxonomy. INTERNAL use ONLY!
	 *
	 * @var bool
	 */
	protected $bool_builtin;

	/**
	 * The class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Sets the default values for the class' properties.
	 */
	protected function setPropertyDefaults() {

		$this->arr_args_base                = array();
		$this->arr_args                     = array();
		$this->mix_set_return               = null;
		$this->str_label_plural             = false;
		$this->arr_labels                   = array();
		$this->bool_public                  = true;
		$this->bool_publicly_queryable      = null;
		$this->bool_show_ui                 = null;
		$this->bool_show_in_menu            = null;
		$this->bool_show_in_nav_menus       = null;
		$this->bool_show_in_rest            = true;
		$this->str_rest_base                = null;
		$this->str_rest_controller_class    = 'WP_REST_Terms_Controller';
		$this->bool_show_tagcloud           = null;
		$this->bool_show_in_quick_edit      = null;
		$this->mix_meta_box_cb              = null;
		$this->str_meta_box_sanitize_cb     = null;
		$this->bool_show_admin_column       = false;
		$this->str_description              = '';
		$this->bool_hierarchical            = false;
		$this->str_update_count_callback    = null;
		$this->mix_query_var                = null;
		$this->mix_rewrite                  = true;
		$this->arr_capabilities             = array();
		$this->bool_sort                    = null;
		$this->arr_wp_get_object_terms_args = null;
		$this->bool_builtin                 = false;

	}

	/**
	 * Set property: $str_label_plural.
	 *
	 * @param string $str Taxonomy label, singular.
	 *
	 * @return void
	 */
	public function setLabel( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_label_plural', $str );
	}

	/**
	 * Set property: $arr_labels
	 *
	 * @param array $arr Value of taxonomy arg: 'labels'.
	 *
	 * @return void
	 */
	public function setLabels( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_labels', $arr );
	}

	/**
	 * Set property: $arr_args.
	 *
	 * @param array $arr Array of taxonomy args.
	 *
	 * @return void
	 */
	public function setArgs( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_args', $arr );
	}

	/**
	 * Set property: $bool_public. Default: true.
	 *
	 * @param boolean $bool Value of taxonomy arg: 'public'.
	 *
	 * @return void
	 */
	public function setPublic( bool $bool = true ) {

		$this->mix_set_return = $this->setBool( 'bool_public', $bool );
	}

	/**
	 * Set property: $bool_publicly_queryable. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'publicly_queryable'.
	 *
	 * @return void
	 */
	public function setPubliclyQueryable( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_publicly_queryable', $bool );
	}

	/**
	 * Set property: $bool_show_ui. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_ui'.
	 *
	 * @return void
	 */
	public function setShowUI( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_show_ui', $bool );
	}

	/**
	 * Set property: $bool_show_in_menu. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_in_menu'.
	 *
	 * @return void
	 */
	public function setShowInMenu( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_menu', $bool );
	}

	/**
	 * Set property: $bool_show_in_nav_menus. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_in_nav_menus'.
	 *
	 * @return void
	 */
	public function setShowInNavMenus( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_nav_menus', $bool );
	}

	/**
	 * Set property: $bool_show_in_rest. Default: true. (note: WP's default is false)
	 *
	 * @param boolean $bool Value of taxonomy arg: 'show_in_rest'.
	 *
	 * @return void
	 */
	public function setShowInRest( bool $bool = true ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_rest', $bool );
	}

	/**
	 * Set property: $str_rest_base. Default: null.
	 *
	 * @param string $str Value of taxonomy arg: 'rest_base'.
	 *
	 * @return void
	 */
	public function setRestBase( string $str = null ) {

		$this->mix_set_return = $this->setString( 'str_rest_base', $str );
	}

	/**
	 * Set property: $str_rest_controller_class. Default: 'WP_REST_Terms_Controller'.
	 *
	 * @param string $str Value of taxonomy arg: 'rest_controller_class'.
	 *
	 * @return void
	 */
	public function setRestControllerClass( string $str = 'WP_REST_Terms_Controller' ) {

		$this->mix_set_return = $this->setString( 'str_rest_controller_class', $str );
	}

	/**
	 * Set property: $bool_show_tagcloud. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_tagcloud'.
	 *
	 * @return void
	 */
	public function setShowTagcloud( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_show_tagcloud', $bool );
	}

	/**
	 * Set property: $bool_show_in_quick_edit. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_in_quick_edit'.
	 *
	 * @return void
	 */
	public function setShowInQuickEdit( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_quick_edit', $bool );
	}

	/**
	 * Set property: $mix_meta_box_cb. Default: null.
	 *
	 * @param false|string $mix Value of taxonomy arg: 'meta_box_cb'.
	 *
	 * @return void
	 */
	public function setMetaBoxCB( $mix = null ) {

		$this->mix_set_return = false;

		if ( false === $mix ) {

			$this->mix_set_return = $this->setBool( 'mix_meta_box_cb', $mix );

		} elseif ( is_string( $mix ) ) {

			$this->mix_set_return = $this->setString( 'mix_meta_box_cb', $mix );
		}
	}


	/**
	 * Set property: $str_meta_box_sanitize_cb. Default: null.
	 *
	 * @param string $str Value of taxonomy arg: 'meta_box_sanitize_cb'.
	 *
	 * @return void
	 */
	public function setMetaBoxSanitizeCB( string $str = null ) {

		$this->mix_set_return = $this->setString( 'str_meta_box_sanitize_cb', $str );
	}

	/**
	 * Set property: $bool_show_admin_column. Default: false.
	 *
	 * @param bool $bool Value of taxonomy arg: 'show_admin_column'.
	 *
	 * @return void
	 */
	public function setShowAdminColumn( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_admin_column', $bool );
	}

	/**
	 * Set property: $str_description. Default: ''.
	 *
	 * @param string $str Value of taxonomy arg: 'description'.
	 *
	 * @return void
	 */
	public function setDescription( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_description', $str );
	}

	/**
	 * Set property: $bool_hierarchical. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'hierarchical'.
	 *
	 * @return void
	 */
	public function setHierarchical( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_hierarchical', $bool );
	}

	/**
	 * Set property: $str_update_count_callback. Default: null.
	 *
	 * @param string $str Value of taxonomy arg: 'update_count_callback'.
	 *
	 * @return void
	 */
	public function setUpdateCountCallback( string $str = null ) {

		$this->mix_set_return = $this->setString( 'str_update_count_callback', $str );
	}

	/**
	 * Set property: $mix_query_var. Default: $taxonomy.
	 *
	 * @param bool|string $mix Value of taxonomy arg: 'query_var'.
	 *
	 * @return void
	 */
	public function setQueryVar( $mix = null ) {

		if ( is_bool( $mix ) || is_string( $mix ) ) {

			$this->mix_query_var  = $mix;
			$this->mix_set_return = true;
		}

		$this->mix_set_return = false;
	}

	/**
	 * Set property: $mix_rewrite. Default: true.
	 *
	 * @param bool|array $mix Value of taxonomy arg: 'rewrite'.
	 *
	 * @return void
	 */
	public function setRewrite( $mix = true ) {

		$this->mix_set_return = false;
		if ( is_bool( $mix ) || is_array( $mix ) ) {

			$this->mix_rewrite    = $mix;
			$this->mix_set_return = true;
		}
	}

	/**
	 * Set property: $arr_capabilities. Default: false.
	 *
	 * @param array $arr Value of taxonomy arg: 'public'.
	 *
	 * @return void
	 */
	public function setCapabilities( array $arr = array() ) {

		$this->mix_set_return = false;
		if ( ! empty( $arr ) ) {
			$this->mix_set_return = $this->setArray( 'arr_capabilities', $arr );
		}
	}

	/**
	 * Set property: $bool_sort. Default: null.
	 *
	 * @param bool $bool Value of taxonomy arg: 'sort'.
	 *
	 * @return void
	 */
	public function setSort( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_sort', $bool );
	}

	/**
	 * Set property: $arr_wp_get_object_terms_args. Default: null.
	 *
	 * @param array $arr Value of taxonomy arg: 'args'.
	 *
	 * @return void
	 */
	public function setWPGetObjectTermsArgs( array $arr = array() ) {

		$this->mix_set_return = false;
		if ( ! empty( $arr ) ) {
			$this->mix_set_return = $this->setArray( 'arr_wp_get_object_terms_args', $arr );
		}
	}

	/**
	 * Set property: $bool_builtin. Default: false.
	 *
	 * @param bool $bool Value of taxonomy arg: 'builtin'.
	 *
	 * @return void
	 */
	public function setBuiltIn( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_builtin', $bool );
	}

	/**
	 * Set property: $arr_args_base. Default: array().
	 *
	 * @param array $arr Value of property: $arr_args_base.
	 *
	 * @return void
	 */
	public function loadArgsBase( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_args_base', $arr );
	}

	/**
	 * Set property: $arr_args. Default: array().
	 *
	 * @param array $arr The args for this taxonomy.
	 *
	 * @return void
	 */
	public function loadArgs( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_args', $arr );
	}

	/**
	 * Update the property: $arr_args. Default: array().
	 *
	 * @param array $arr The args to array_merge() over the property: $arr_args.
	 *
	 * @return void
	 */
	public function updateArgs( array $arr = array() ) {

		$this->mix_set_return = false;
		if ( is_array( $arr ) ) {

			$this->arr_args       = array_merge( $this->arr_args, $arr );
			$this->mix_set_return = true;
		}
	}

	/**
	 * Get property: $arr_args.
	 *
	 * @param array $arr Last chance args to array_merge() over the other args-centric properties.
	 *
	 * @return array
	 */
	public function getArgsAll( array $arr = array() ) {

		$args_all = array_merge( $this->getArgsDefaults(), $this->arr_args_base, $this->arr_args, $arr );

		// Filter null values from the associative array.
		$args_all = array_filter(
			$args_all,
			function( $value, $key ) {
				return null !== $value;
			},
			ARRAY_FILTER_USE_BOTH
		);
		return $args_all;
	}

	/**
	 * Returns WP's default args for registering a taxonomy.
	 *
	 * @return array
	 */
	protected function getArgsDefaults() {

		$args = array(

			/*
				* (string) (optional) A plural descriptive name for the post type marked for translation.
				*
				* - Default: Value of $labels['name']
				*/
			'label'                 => $this->str_label_plural,

			/*
				* (array) (optional) labels - An array of labels for this post type. By default, post labels are used
				* for non-hierarchical post types and page labels for hierarchical ones.
				*
				* - Default: if empty, 'name' is set to value of 'label', and 'singular_name' is set to value of 'name'.
				*/
			'labels'                => $this->arr_labels,

			/*
				* (boolean) (optional) Whether a taxonomy is intended for use publicly either via the admin interface
				* or by front-end users. The default settings of `$publicly_queryable`, `$show_ui`, and `$show_in_nav_menus` are inherited from `$public`.
				*/
			'public'                => $this->bool_public,

			// (boolean) (optional) Whether the taxonomy is publicly queryable.
			'publicly_queryable'    => $this->bool_publicly_queryable,

			// (boolean) (optional) Whether to generate a default UI for managing this taxonomy.
			// Default: if not set, defaults to value of public argument. As of 3.5, setting this to false for attachment taxonomies will hide the UI.
			'show_ui'               => $this->bool_show_ui,

			// (boolean) (optional) Where to show the taxonomy in the admin menu. show_ui must be true.
			// Default: value of show_ui argument
			// false - do not display in the admin menu
			// true - show as a submenu of associated object types
			'show_in_menu'          => $this->bool_show_in_menu,

			// (boolean) (optional) true makes this taxonomy available for selection in navigation menus.
			// Default: if not set, defaults to value of public argument
			'show_in_nav_menus'     => $this->bool_show_in_nav_menus,

			// (boolean) (optional) Whether to include the taxonomy in the REST API.
			// Default: true (WP's default: false; but that pre-dates the REST API, and false means the taxonomy is not in the block editor).
			'show_in_rest'          => $this->bool_show_in_rest,

			// (string) (optional) To change the base url of REST API route.
			// Default: $taxonomy
			'rest_base'             => $this->str_rest_base,

			// (string) (optional) REST API Controller class name.
			// Default: WP_REST_Terms_Controller
			'rest_controller_class' => $this->str_rest_controller_class,

			// (boolean) (optional) Whether to allow the Tag Cloud widget to use this taxonomy.
			// Default: if not set, defaults to value of show_ui argument
			'show_tagcloud'         => $this->bool_show_tagcloud,

			// (boolean) (optional) Whether to show the taxonomy in the quick/bulk edit panel. (Available since 4.2)
			// Default: if not set, defaults to value of show_ui argument.
			'show_in_quick_edit'    => $this->bool_show_in_quick_edit,

			// (callback) (optional) Provide a callback function name for the meta box display. (Available since 3.8)
			// Default: null
			// Note: Defaults to the categories meta box (post_categories_meta_box() in meta-boxes.php) for hierarchical taxonomies and the
			// tags meta box (post_tags_meta_box()) for non-hierarchical taxonomies. No meta box is shown if set to false.
			'meta_box_cb'           => $this->mix_meta_box_cb,

			// Callback function for sanitizing taxonomy data saved from a meta box.
			// if no callback is defined, an appropriate one is determined based on the value of $meta_box_cb.
			'meta_box_sanitize_cb'  => $this->str_meta_box_sanitize_cb,

			// (boolean) (optional) Whether to allow automatic creation of taxonomy columns on associated post-types table. (Available since 3.5)
			// Default: false
			'show_admin_column'     => $this->bool_show_admin_column,

			// (string) (optional) Include a description of the taxonomy.
			// Default: ""
			'description'           => $this->str_description,

			// (boolean) (optional) Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.
			// Default: false
			// Note: Hierarchical taxonomies will have a list with checkboxes to select an existing category in the taxonomy admin box on the post edit page (like default post categories). Non-hierarchical taxonomies will just have an empty text field to type-in taxonomy terms to associate with the post (like default post tags).
			'hierarchical'          => $this->bool_hierarchical,

			// (string) (optional) A function name that will be called when the count of an associated $object_type, such as post, is updated. Works much like a hook.
			// Default: None - but see Note
			// Note: For all details please see: https://codex.wordpress.org/Function_Reference/register_taxonomy
			'update_count_callback' => $this->str_update_count_callback,

			// (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var instead of default which is $taxonomy, the taxonomy's "name". True is not seen as a valid entry and will result in 404 issues.
			// Default: $taxonomy
			// Note: The query_var is used for direct queries through WP_Query like new WP_Query(array('people'=>$person_name)) and URL queries
			// like /?people=$person_name. Setting query_var to false will disable these methods, but you can still fetch posts with an
			// explicit WP_Query taxonomy query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
			'query_var'             => $this->mix_query_var,

			// (boolean/array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks". Pass an $args array to override default URL settings for permalinks as outlined below:
			// Default: true
			// Note: For all details please see: https://codex.wordpress.org/Function_Reference/register_taxonomy
			'rewrite'               => $this->mix_rewrite,

			// (array) (optional) An array of the capabilities for this taxonomy.
			// Default: None
			'capabilities'          => $this->arr_capabilities,

			// (boolean) (optional) Whether this taxonomy should remember the order in which terms are added to objects.
			// Default: None
			'sort'                  => $this->bool_sort,

			// array of arguments to automatically use inside wp_get_object_terms() for this taxonomy.
			'args'                  => $this->arr_wp_get_object_terms_args,

			// (boolean) (not for general use) Whether this taxonomy is a native or "built-in" taxonomy. Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own taxonomy
			// Default: false
			'_builtin'              => $this->bool_builtin,
		);

		return $args;
	}

	/**
	 * Helper method of setting a property with any expected value of type array.
	 *
	 * @param string $str_prop      The property to set.
	 * @param array  $arr           The value (of type array) to set.
	 * @param int    $int_min_count The minimum number of items in the array.
	 * @param int    $int_max_count The maximum number of items in the array. false = no limit.
	 *
	 * @return bool
	 */
	protected function setArray( string $str_prop = '', array $arr = array(), $int_min_count = 0, $int_max_count = false ) {

		if ( property_exists( $this, $str_prop )
			&& count( $arr ) >= absint( $int_min_count )
			&& ( false === $int_max_count || count( $arr ) <= absint( $int_max_count ) ) ) {

			$this->$str_prop = $arr;

			return true;
		}
		return false;
	}

	/**
	 * Helper method of setting a property with any expected value of type boolean.
	 *
	 * @param string $str_prop The property to set.
	 * @param bool   $bool     The value (of type boolean) to set.
	 * @param array  $arr_flags An array of flags to pass to filter_var().
	 *
	 * @return bool
	 */
	protected function setBool( string $str_prop = '', bool $bool = null, array $arr_flags = array() ) {

		if ( property_exists( $this, $str_prop ) ) {

			// http://php.net/manual/en/filter.filters.validate.php .
			if ( is_array( $arr_flags ) && filter_var( $bool, FILTER_VALIDATE_BOOLEAN, $arr_flags ) ) {

				$this->$str_prop = $bool;
				return true;

			} else {

				$this->$str_prop = $bool;
				return true;
			}
			return false;
		}
	}

	/**
	 * Helper method of setting a property with any expected value of type string.
	 *
	 * @param string $str_prop   The property to set.
	 * @param string $str        The value (of type string) to set.
	 * @param array  $arr_len    An array of min and max lengths.
	 * @param bool   $bool_ltrim Whether to trim the string before checking the length.
	 *
	 * @return bool
	 */
	protected function setString( string $str_prop = '', string $str = '', array $arr_len = array(), bool $bool_ltrim = true ) {

		$arr_len_default = array(
			'min_len' => 0,
			'max_len' => false,
		);

		$arr_len_default = array_merge( $arr_len_default, $arr_len );

		if ( property_exists( $this, $str_prop ) && is_string( $str ) ) {

			// without this, e.g., 3x spaces would have a strlen = 3.
			if ( $bool_ltrim && empty( ltrim( $str ) ) ) {
				$str = '';
			}

			if ( strlen( $str ) >= absint( $arr_len_default['min_len'] )
				&& ( false === $arr_len_default['max_len']
				|| absint( $arr_len_default['max_len'] ) >= strlen( $str ) ) ) {

				$this->$str_prop = $str;
				return true;
			}
			return false;
		}
	}
}
